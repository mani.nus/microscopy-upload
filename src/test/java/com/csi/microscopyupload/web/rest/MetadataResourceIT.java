package com.csi.microscopyupload.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.csi.microscopyupload.IntegrationTest;
import com.csi.microscopyupload.domain.Metadata;
import com.csi.microscopyupload.repository.MetadataRepository;
import com.csi.microscopyupload.service.criteria.MetadataCriteria;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MetadataResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MetadataResourceIT {

    private static final String DEFAULT_CELL_LINE = "AAAAAAAAAA";
    private static final String UPDATED_CELL_LINE = "BBBBBBBBBB";

    private static final String DEFAULT_ANTIBODIES_USED = "AAAAAAAAAA";
    private static final String UPDATED_ANTIBODIES_USED = "BBBBBBBBBB";

    private static final String DEFAULT_FIXATION_CONDITIONS = "AAAAAAAAAA";
    private static final String UPDATED_FIXATION_CONDITIONS = "BBBBBBBBBB";

    private static final String DEFAULT_EXPERIMENTAL_INVENTION = "AAAAAAAAAA";
    private static final String UPDATED_EXPERIMENTAL_INVENTION = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/metadata";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MetadataRepository metadataRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMetadataMockMvc;

    private Metadata metadata;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Metadata createEntity(EntityManager em) {
        Metadata metadata = new Metadata()
            .cellLine(DEFAULT_CELL_LINE)
            .antibodiesUsed(DEFAULT_ANTIBODIES_USED)
            .fixationConditions(DEFAULT_FIXATION_CONDITIONS)
            .experimentalInvention(DEFAULT_EXPERIMENTAL_INVENTION);
        return metadata;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Metadata createUpdatedEntity(EntityManager em) {
        Metadata metadata = new Metadata()
            .cellLine(UPDATED_CELL_LINE)
            .antibodiesUsed(UPDATED_ANTIBODIES_USED)
            .fixationConditions(UPDATED_FIXATION_CONDITIONS)
            .experimentalInvention(UPDATED_EXPERIMENTAL_INVENTION);
        return metadata;
    }

    @BeforeEach
    public void initTest() {
        metadata = createEntity(em);
    }

    @Test
    @Transactional
    void createMetadata() throws Exception {
        int databaseSizeBeforeCreate = metadataRepository.findAll().size();
        // Create the Metadata
        restMetadataMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(metadata)))
            .andExpect(status().isCreated());

        // Validate the Metadata in the database
        List<Metadata> metadataList = metadataRepository.findAll();
        assertThat(metadataList).hasSize(databaseSizeBeforeCreate + 1);
        Metadata testMetadata = metadataList.get(metadataList.size() - 1);
        assertThat(testMetadata.getCellLine()).isEqualTo(DEFAULT_CELL_LINE);
        assertThat(testMetadata.getAntibodiesUsed()).isEqualTo(DEFAULT_ANTIBODIES_USED);
        assertThat(testMetadata.getFixationConditions()).isEqualTo(DEFAULT_FIXATION_CONDITIONS);
        assertThat(testMetadata.getExperimentalInvention()).isEqualTo(DEFAULT_EXPERIMENTAL_INVENTION);
    }

    @Test
    @Transactional
    void createMetadataWithExistingId() throws Exception {
        // Create the Metadata with an existing ID
        metadata.setId(1L);

        int databaseSizeBeforeCreate = metadataRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMetadataMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(metadata)))
            .andExpect(status().isBadRequest());

        // Validate the Metadata in the database
        List<Metadata> metadataList = metadataRepository.findAll();
        assertThat(metadataList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkCellLineIsRequired() throws Exception {
        int databaseSizeBeforeTest = metadataRepository.findAll().size();
        // set the field null
        metadata.setCellLine(null);

        // Create the Metadata, which fails.

        restMetadataMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(metadata)))
            .andExpect(status().isBadRequest());

        List<Metadata> metadataList = metadataRepository.findAll();
        assertThat(metadataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllMetadata() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList
        restMetadataMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(metadata.getId().intValue())))
            .andExpect(jsonPath("$.[*].cellLine").value(hasItem(DEFAULT_CELL_LINE)))
            .andExpect(jsonPath("$.[*].antibodiesUsed").value(hasItem(DEFAULT_ANTIBODIES_USED)))
            .andExpect(jsonPath("$.[*].fixationConditions").value(hasItem(DEFAULT_FIXATION_CONDITIONS)))
            .andExpect(jsonPath("$.[*].experimentalInvention").value(hasItem(DEFAULT_EXPERIMENTAL_INVENTION)));
    }

    @Test
    @Transactional
    void getMetadata() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get the metadata
        restMetadataMockMvc
            .perform(get(ENTITY_API_URL_ID, metadata.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(metadata.getId().intValue()))
            .andExpect(jsonPath("$.cellLine").value(DEFAULT_CELL_LINE))
            .andExpect(jsonPath("$.antibodiesUsed").value(DEFAULT_ANTIBODIES_USED))
            .andExpect(jsonPath("$.fixationConditions").value(DEFAULT_FIXATION_CONDITIONS))
            .andExpect(jsonPath("$.experimentalInvention").value(DEFAULT_EXPERIMENTAL_INVENTION));
    }

    @Test
    @Transactional
    void getMetadataByIdFiltering() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        Long id = metadata.getId();

        defaultMetadataShouldBeFound("id.equals=" + id);
        defaultMetadataShouldNotBeFound("id.notEquals=" + id);

        defaultMetadataShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultMetadataShouldNotBeFound("id.greaterThan=" + id);

        defaultMetadataShouldBeFound("id.lessThanOrEqual=" + id);
        defaultMetadataShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllMetadataByCellLineIsEqualToSomething() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where cellLine equals to DEFAULT_CELL_LINE
        defaultMetadataShouldBeFound("cellLine.equals=" + DEFAULT_CELL_LINE);

        // Get all the metadataList where cellLine equals to UPDATED_CELL_LINE
        defaultMetadataShouldNotBeFound("cellLine.equals=" + UPDATED_CELL_LINE);
    }

    @Test
    @Transactional
    void getAllMetadataByCellLineIsNotEqualToSomething() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where cellLine not equals to DEFAULT_CELL_LINE
        defaultMetadataShouldNotBeFound("cellLine.notEquals=" + DEFAULT_CELL_LINE);

        // Get all the metadataList where cellLine not equals to UPDATED_CELL_LINE
        defaultMetadataShouldBeFound("cellLine.notEquals=" + UPDATED_CELL_LINE);
    }

    @Test
    @Transactional
    void getAllMetadataByCellLineIsInShouldWork() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where cellLine in DEFAULT_CELL_LINE or UPDATED_CELL_LINE
        defaultMetadataShouldBeFound("cellLine.in=" + DEFAULT_CELL_LINE + "," + UPDATED_CELL_LINE);

        // Get all the metadataList where cellLine equals to UPDATED_CELL_LINE
        defaultMetadataShouldNotBeFound("cellLine.in=" + UPDATED_CELL_LINE);
    }

    @Test
    @Transactional
    void getAllMetadataByCellLineIsNullOrNotNull() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where cellLine is not null
        defaultMetadataShouldBeFound("cellLine.specified=true");

        // Get all the metadataList where cellLine is null
        defaultMetadataShouldNotBeFound("cellLine.specified=false");
    }

    @Test
    @Transactional
    void getAllMetadataByCellLineContainsSomething() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where cellLine contains DEFAULT_CELL_LINE
        defaultMetadataShouldBeFound("cellLine.contains=" + DEFAULT_CELL_LINE);

        // Get all the metadataList where cellLine contains UPDATED_CELL_LINE
        defaultMetadataShouldNotBeFound("cellLine.contains=" + UPDATED_CELL_LINE);
    }

    @Test
    @Transactional
    void getAllMetadataByCellLineNotContainsSomething() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where cellLine does not contain DEFAULT_CELL_LINE
        defaultMetadataShouldNotBeFound("cellLine.doesNotContain=" + DEFAULT_CELL_LINE);

        // Get all the metadataList where cellLine does not contain UPDATED_CELL_LINE
        defaultMetadataShouldBeFound("cellLine.doesNotContain=" + UPDATED_CELL_LINE);
    }

    @Test
    @Transactional
    void getAllMetadataByAntibodiesUsedIsEqualToSomething() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where antibodiesUsed equals to DEFAULT_ANTIBODIES_USED
        defaultMetadataShouldBeFound("antibodiesUsed.equals=" + DEFAULT_ANTIBODIES_USED);

        // Get all the metadataList where antibodiesUsed equals to UPDATED_ANTIBODIES_USED
        defaultMetadataShouldNotBeFound("antibodiesUsed.equals=" + UPDATED_ANTIBODIES_USED);
    }

    @Test
    @Transactional
    void getAllMetadataByAntibodiesUsedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where antibodiesUsed not equals to DEFAULT_ANTIBODIES_USED
        defaultMetadataShouldNotBeFound("antibodiesUsed.notEquals=" + DEFAULT_ANTIBODIES_USED);

        // Get all the metadataList where antibodiesUsed not equals to UPDATED_ANTIBODIES_USED
        defaultMetadataShouldBeFound("antibodiesUsed.notEquals=" + UPDATED_ANTIBODIES_USED);
    }

    @Test
    @Transactional
    void getAllMetadataByAntibodiesUsedIsInShouldWork() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where antibodiesUsed in DEFAULT_ANTIBODIES_USED or UPDATED_ANTIBODIES_USED
        defaultMetadataShouldBeFound("antibodiesUsed.in=" + DEFAULT_ANTIBODIES_USED + "," + UPDATED_ANTIBODIES_USED);

        // Get all the metadataList where antibodiesUsed equals to UPDATED_ANTIBODIES_USED
        defaultMetadataShouldNotBeFound("antibodiesUsed.in=" + UPDATED_ANTIBODIES_USED);
    }

    @Test
    @Transactional
    void getAllMetadataByAntibodiesUsedIsNullOrNotNull() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where antibodiesUsed is not null
        defaultMetadataShouldBeFound("antibodiesUsed.specified=true");

        // Get all the metadataList where antibodiesUsed is null
        defaultMetadataShouldNotBeFound("antibodiesUsed.specified=false");
    }

    @Test
    @Transactional
    void getAllMetadataByAntibodiesUsedContainsSomething() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where antibodiesUsed contains DEFAULT_ANTIBODIES_USED
        defaultMetadataShouldBeFound("antibodiesUsed.contains=" + DEFAULT_ANTIBODIES_USED);

        // Get all the metadataList where antibodiesUsed contains UPDATED_ANTIBODIES_USED
        defaultMetadataShouldNotBeFound("antibodiesUsed.contains=" + UPDATED_ANTIBODIES_USED);
    }

    @Test
    @Transactional
    void getAllMetadataByAntibodiesUsedNotContainsSomething() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where antibodiesUsed does not contain DEFAULT_ANTIBODIES_USED
        defaultMetadataShouldNotBeFound("antibodiesUsed.doesNotContain=" + DEFAULT_ANTIBODIES_USED);

        // Get all the metadataList where antibodiesUsed does not contain UPDATED_ANTIBODIES_USED
        defaultMetadataShouldBeFound("antibodiesUsed.doesNotContain=" + UPDATED_ANTIBODIES_USED);
    }

    @Test
    @Transactional
    void getAllMetadataByFixationConditionsIsEqualToSomething() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where fixationConditions equals to DEFAULT_FIXATION_CONDITIONS
        defaultMetadataShouldBeFound("fixationConditions.equals=" + DEFAULT_FIXATION_CONDITIONS);

        // Get all the metadataList where fixationConditions equals to UPDATED_FIXATION_CONDITIONS
        defaultMetadataShouldNotBeFound("fixationConditions.equals=" + UPDATED_FIXATION_CONDITIONS);
    }

    @Test
    @Transactional
    void getAllMetadataByFixationConditionsIsNotEqualToSomething() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where fixationConditions not equals to DEFAULT_FIXATION_CONDITIONS
        defaultMetadataShouldNotBeFound("fixationConditions.notEquals=" + DEFAULT_FIXATION_CONDITIONS);

        // Get all the metadataList where fixationConditions not equals to UPDATED_FIXATION_CONDITIONS
        defaultMetadataShouldBeFound("fixationConditions.notEquals=" + UPDATED_FIXATION_CONDITIONS);
    }

    @Test
    @Transactional
    void getAllMetadataByFixationConditionsIsInShouldWork() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where fixationConditions in DEFAULT_FIXATION_CONDITIONS or UPDATED_FIXATION_CONDITIONS
        defaultMetadataShouldBeFound("fixationConditions.in=" + DEFAULT_FIXATION_CONDITIONS + "," + UPDATED_FIXATION_CONDITIONS);

        // Get all the metadataList where fixationConditions equals to UPDATED_FIXATION_CONDITIONS
        defaultMetadataShouldNotBeFound("fixationConditions.in=" + UPDATED_FIXATION_CONDITIONS);
    }

    @Test
    @Transactional
    void getAllMetadataByFixationConditionsIsNullOrNotNull() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where fixationConditions is not null
        defaultMetadataShouldBeFound("fixationConditions.specified=true");

        // Get all the metadataList where fixationConditions is null
        defaultMetadataShouldNotBeFound("fixationConditions.specified=false");
    }

    @Test
    @Transactional
    void getAllMetadataByFixationConditionsContainsSomething() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where fixationConditions contains DEFAULT_FIXATION_CONDITIONS
        defaultMetadataShouldBeFound("fixationConditions.contains=" + DEFAULT_FIXATION_CONDITIONS);

        // Get all the metadataList where fixationConditions contains UPDATED_FIXATION_CONDITIONS
        defaultMetadataShouldNotBeFound("fixationConditions.contains=" + UPDATED_FIXATION_CONDITIONS);
    }

    @Test
    @Transactional
    void getAllMetadataByFixationConditionsNotContainsSomething() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where fixationConditions does not contain DEFAULT_FIXATION_CONDITIONS
        defaultMetadataShouldNotBeFound("fixationConditions.doesNotContain=" + DEFAULT_FIXATION_CONDITIONS);

        // Get all the metadataList where fixationConditions does not contain UPDATED_FIXATION_CONDITIONS
        defaultMetadataShouldBeFound("fixationConditions.doesNotContain=" + UPDATED_FIXATION_CONDITIONS);
    }

    @Test
    @Transactional
    void getAllMetadataByExperimentalInventionIsEqualToSomething() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where experimentalInvention equals to DEFAULT_EXPERIMENTAL_INVENTION
        defaultMetadataShouldBeFound("experimentalInvention.equals=" + DEFAULT_EXPERIMENTAL_INVENTION);

        // Get all the metadataList where experimentalInvention equals to UPDATED_EXPERIMENTAL_INVENTION
        defaultMetadataShouldNotBeFound("experimentalInvention.equals=" + UPDATED_EXPERIMENTAL_INVENTION);
    }

    @Test
    @Transactional
    void getAllMetadataByExperimentalInventionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where experimentalInvention not equals to DEFAULT_EXPERIMENTAL_INVENTION
        defaultMetadataShouldNotBeFound("experimentalInvention.notEquals=" + DEFAULT_EXPERIMENTAL_INVENTION);

        // Get all the metadataList where experimentalInvention not equals to UPDATED_EXPERIMENTAL_INVENTION
        defaultMetadataShouldBeFound("experimentalInvention.notEquals=" + UPDATED_EXPERIMENTAL_INVENTION);
    }

    @Test
    @Transactional
    void getAllMetadataByExperimentalInventionIsInShouldWork() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where experimentalInvention in DEFAULT_EXPERIMENTAL_INVENTION or UPDATED_EXPERIMENTAL_INVENTION
        defaultMetadataShouldBeFound("experimentalInvention.in=" + DEFAULT_EXPERIMENTAL_INVENTION + "," + UPDATED_EXPERIMENTAL_INVENTION);

        // Get all the metadataList where experimentalInvention equals to UPDATED_EXPERIMENTAL_INVENTION
        defaultMetadataShouldNotBeFound("experimentalInvention.in=" + UPDATED_EXPERIMENTAL_INVENTION);
    }

    @Test
    @Transactional
    void getAllMetadataByExperimentalInventionIsNullOrNotNull() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where experimentalInvention is not null
        defaultMetadataShouldBeFound("experimentalInvention.specified=true");

        // Get all the metadataList where experimentalInvention is null
        defaultMetadataShouldNotBeFound("experimentalInvention.specified=false");
    }

    @Test
    @Transactional
    void getAllMetadataByExperimentalInventionContainsSomething() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where experimentalInvention contains DEFAULT_EXPERIMENTAL_INVENTION
        defaultMetadataShouldBeFound("experimentalInvention.contains=" + DEFAULT_EXPERIMENTAL_INVENTION);

        // Get all the metadataList where experimentalInvention contains UPDATED_EXPERIMENTAL_INVENTION
        defaultMetadataShouldNotBeFound("experimentalInvention.contains=" + UPDATED_EXPERIMENTAL_INVENTION);
    }

    @Test
    @Transactional
    void getAllMetadataByExperimentalInventionNotContainsSomething() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        // Get all the metadataList where experimentalInvention does not contain DEFAULT_EXPERIMENTAL_INVENTION
        defaultMetadataShouldNotBeFound("experimentalInvention.doesNotContain=" + DEFAULT_EXPERIMENTAL_INVENTION);

        // Get all the metadataList where experimentalInvention does not contain UPDATED_EXPERIMENTAL_INVENTION
        defaultMetadataShouldBeFound("experimentalInvention.doesNotContain=" + UPDATED_EXPERIMENTAL_INVENTION);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMetadataShouldBeFound(String filter) throws Exception {
        restMetadataMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(metadata.getId().intValue())))
            .andExpect(jsonPath("$.[*].cellLine").value(hasItem(DEFAULT_CELL_LINE)))
            .andExpect(jsonPath("$.[*].antibodiesUsed").value(hasItem(DEFAULT_ANTIBODIES_USED)))
            .andExpect(jsonPath("$.[*].fixationConditions").value(hasItem(DEFAULT_FIXATION_CONDITIONS)))
            .andExpect(jsonPath("$.[*].experimentalInvention").value(hasItem(DEFAULT_EXPERIMENTAL_INVENTION)));

        // Check, that the count call also returns 1
        restMetadataMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMetadataShouldNotBeFound(String filter) throws Exception {
        restMetadataMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMetadataMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingMetadata() throws Exception {
        // Get the metadata
        restMetadataMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMetadata() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        int databaseSizeBeforeUpdate = metadataRepository.findAll().size();

        // Update the metadata
        Metadata updatedMetadata = metadataRepository.findById(metadata.getId()).get();
        // Disconnect from session so that the updates on updatedMetadata are not directly saved in db
        em.detach(updatedMetadata);
        updatedMetadata
            .cellLine(UPDATED_CELL_LINE)
            .antibodiesUsed(UPDATED_ANTIBODIES_USED)
            .fixationConditions(UPDATED_FIXATION_CONDITIONS)
            .experimentalInvention(UPDATED_EXPERIMENTAL_INVENTION);

        restMetadataMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedMetadata.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedMetadata))
            )
            .andExpect(status().isOk());

        // Validate the Metadata in the database
        List<Metadata> metadataList = metadataRepository.findAll();
        assertThat(metadataList).hasSize(databaseSizeBeforeUpdate);
        Metadata testMetadata = metadataList.get(metadataList.size() - 1);
        assertThat(testMetadata.getCellLine()).isEqualTo(UPDATED_CELL_LINE);
        assertThat(testMetadata.getAntibodiesUsed()).isEqualTo(UPDATED_ANTIBODIES_USED);
        assertThat(testMetadata.getFixationConditions()).isEqualTo(UPDATED_FIXATION_CONDITIONS);
        assertThat(testMetadata.getExperimentalInvention()).isEqualTo(UPDATED_EXPERIMENTAL_INVENTION);
    }

    @Test
    @Transactional
    void putNonExistingMetadata() throws Exception {
        int databaseSizeBeforeUpdate = metadataRepository.findAll().size();
        metadata.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMetadataMockMvc
            .perform(
                put(ENTITY_API_URL_ID, metadata.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(metadata))
            )
            .andExpect(status().isBadRequest());

        // Validate the Metadata in the database
        List<Metadata> metadataList = metadataRepository.findAll();
        assertThat(metadataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMetadata() throws Exception {
        int databaseSizeBeforeUpdate = metadataRepository.findAll().size();
        metadata.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMetadataMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(metadata))
            )
            .andExpect(status().isBadRequest());

        // Validate the Metadata in the database
        List<Metadata> metadataList = metadataRepository.findAll();
        assertThat(metadataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMetadata() throws Exception {
        int databaseSizeBeforeUpdate = metadataRepository.findAll().size();
        metadata.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMetadataMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(metadata)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Metadata in the database
        List<Metadata> metadataList = metadataRepository.findAll();
        assertThat(metadataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMetadataWithPatch() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        int databaseSizeBeforeUpdate = metadataRepository.findAll().size();

        // Update the metadata using partial update
        Metadata partialUpdatedMetadata = new Metadata();
        partialUpdatedMetadata.setId(metadata.getId());

        partialUpdatedMetadata.antibodiesUsed(UPDATED_ANTIBODIES_USED);

        restMetadataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMetadata.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMetadata))
            )
            .andExpect(status().isOk());

        // Validate the Metadata in the database
        List<Metadata> metadataList = metadataRepository.findAll();
        assertThat(metadataList).hasSize(databaseSizeBeforeUpdate);
        Metadata testMetadata = metadataList.get(metadataList.size() - 1);
        assertThat(testMetadata.getCellLine()).isEqualTo(DEFAULT_CELL_LINE);
        assertThat(testMetadata.getAntibodiesUsed()).isEqualTo(UPDATED_ANTIBODIES_USED);
        assertThat(testMetadata.getFixationConditions()).isEqualTo(DEFAULT_FIXATION_CONDITIONS);
        assertThat(testMetadata.getExperimentalInvention()).isEqualTo(DEFAULT_EXPERIMENTAL_INVENTION);
    }

    @Test
    @Transactional
    void fullUpdateMetadataWithPatch() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        int databaseSizeBeforeUpdate = metadataRepository.findAll().size();

        // Update the metadata using partial update
        Metadata partialUpdatedMetadata = new Metadata();
        partialUpdatedMetadata.setId(metadata.getId());

        partialUpdatedMetadata
            .cellLine(UPDATED_CELL_LINE)
            .antibodiesUsed(UPDATED_ANTIBODIES_USED)
            .fixationConditions(UPDATED_FIXATION_CONDITIONS)
            .experimentalInvention(UPDATED_EXPERIMENTAL_INVENTION);

        restMetadataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMetadata.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMetadata))
            )
            .andExpect(status().isOk());

        // Validate the Metadata in the database
        List<Metadata> metadataList = metadataRepository.findAll();
        assertThat(metadataList).hasSize(databaseSizeBeforeUpdate);
        Metadata testMetadata = metadataList.get(metadataList.size() - 1);
        assertThat(testMetadata.getCellLine()).isEqualTo(UPDATED_CELL_LINE);
        assertThat(testMetadata.getAntibodiesUsed()).isEqualTo(UPDATED_ANTIBODIES_USED);
        assertThat(testMetadata.getFixationConditions()).isEqualTo(UPDATED_FIXATION_CONDITIONS);
        assertThat(testMetadata.getExperimentalInvention()).isEqualTo(UPDATED_EXPERIMENTAL_INVENTION);
    }

    @Test
    @Transactional
    void patchNonExistingMetadata() throws Exception {
        int databaseSizeBeforeUpdate = metadataRepository.findAll().size();
        metadata.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMetadataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, metadata.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(metadata))
            )
            .andExpect(status().isBadRequest());

        // Validate the Metadata in the database
        List<Metadata> metadataList = metadataRepository.findAll();
        assertThat(metadataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMetadata() throws Exception {
        int databaseSizeBeforeUpdate = metadataRepository.findAll().size();
        metadata.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMetadataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(metadata))
            )
            .andExpect(status().isBadRequest());

        // Validate the Metadata in the database
        List<Metadata> metadataList = metadataRepository.findAll();
        assertThat(metadataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMetadata() throws Exception {
        int databaseSizeBeforeUpdate = metadataRepository.findAll().size();
        metadata.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMetadataMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(metadata)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Metadata in the database
        List<Metadata> metadataList = metadataRepository.findAll();
        assertThat(metadataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMetadata() throws Exception {
        // Initialize the database
        metadataRepository.saveAndFlush(metadata);

        int databaseSizeBeforeDelete = metadataRepository.findAll().size();

        // Delete the metadata
        restMetadataMockMvc
            .perform(delete(ENTITY_API_URL_ID, metadata.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Metadata> metadataList = metadataRepository.findAll();
        assertThat(metadataList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
