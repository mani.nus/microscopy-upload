package com.csi.microscopyupload.service;

import com.csi.microscopyupload.domain.*; // for static metamodels
import com.csi.microscopyupload.domain.Metadata;
import com.csi.microscopyupload.repository.MetadataRepository;
import com.csi.microscopyupload.service.criteria.MetadataCriteria;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Metadata} entities in the database.
 * The main input is a {@link MetadataCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Metadata} or a {@link Page} of {@link Metadata} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MetadataQueryService extends QueryService<Metadata> {

    private final Logger log = LoggerFactory.getLogger(MetadataQueryService.class);

    private final MetadataRepository metadataRepository;

    public MetadataQueryService(MetadataRepository metadataRepository) {
        this.metadataRepository = metadataRepository;
    }

    /**
     * Return a {@link List} of {@link Metadata} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Metadata> findByCriteria(MetadataCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Metadata> specification = createSpecification(criteria);
        return metadataRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Metadata} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Metadata> findByCriteria(MetadataCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Metadata> specification = createSpecification(criteria);
        return metadataRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MetadataCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Metadata> specification = createSpecification(criteria);
        return metadataRepository.count(specification);
    }

    /**
     * Function to convert {@link MetadataCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Metadata> createSpecification(MetadataCriteria criteria) {
        Specification<Metadata> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Metadata_.id));
            }
            if (criteria.getCellLine() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCellLine(), Metadata_.cellLine));
            }
            if (criteria.getAntibodiesUsed() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAntibodiesUsed(), Metadata_.antibodiesUsed));
            }
            if (criteria.getFixationConditions() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFixationConditions(), Metadata_.fixationConditions));
            }
            if (criteria.getExperimentalInvention() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getExperimentalInvention(), Metadata_.experimentalInvention));
            }
        }
        return specification;
    }
}
