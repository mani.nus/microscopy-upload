package com.csi.microscopyupload.service;

import com.csi.microscopyupload.domain.Metadata;
import com.csi.microscopyupload.repository.MetadataRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Metadata}.
 */
@Service
@Transactional
public class MetadataService {

    private final Logger log = LoggerFactory.getLogger(MetadataService.class);

    private final MetadataRepository metadataRepository;

    public MetadataService(MetadataRepository metadataRepository) {
        this.metadataRepository = metadataRepository;
    }

    /**
     * Save a metadata.
     *
     * @param metadata the entity to save.
     * @return the persisted entity.
     */
    public Metadata save(Metadata metadata) {
        log.debug("Request to save Metadata : {}", metadata);
        return metadataRepository.save(metadata);
    }

    /**
     * Partially update a metadata.
     *
     * @param metadata the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Metadata> partialUpdate(Metadata metadata) {
        log.debug("Request to partially update Metadata : {}", metadata);

        return metadataRepository
            .findById(metadata.getId())
            .map(existingMetadata -> {
                if (metadata.getCellLine() != null) {
                    existingMetadata.setCellLine(metadata.getCellLine());
                }
                if (metadata.getAntibodiesUsed() != null) {
                    existingMetadata.setAntibodiesUsed(metadata.getAntibodiesUsed());
                }
                if (metadata.getFixationConditions() != null) {
                    existingMetadata.setFixationConditions(metadata.getFixationConditions());
                }
                if (metadata.getExperimentalInvention() != null) {
                    existingMetadata.setExperimentalInvention(metadata.getExperimentalInvention());
                }

                return existingMetadata;
            })
            .map(metadataRepository::save);
    }

    /**
     * Get all the metadata.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Metadata> findAll(Pageable pageable) {
        log.debug("Request to get all Metadata");
        return metadataRepository.findAll(pageable);
    }

    /**
     * Get one metadata by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Metadata> findOne(Long id) {
        log.debug("Request to get Metadata : {}", id);
        return metadataRepository.findById(id);
    }

    /**
     * Delete the metadata by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Metadata : {}", id);
        metadataRepository.deleteById(id);
    }
}
