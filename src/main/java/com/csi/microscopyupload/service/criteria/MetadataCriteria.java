package com.csi.microscopyupload.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.csi.microscopyupload.domain.Metadata} entity. This class is used
 * in {@link com.csi.microscopyupload.web.rest.MetadataResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /metadata?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
public class MetadataCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter cellLine;

    private StringFilter antibodiesUsed;

    private StringFilter fixationConditions;

    private StringFilter experimentalInvention;

    private Boolean distinct;

    public MetadataCriteria() {}

    public MetadataCriteria(MetadataCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.cellLine = other.cellLine == null ? null : other.cellLine.copy();
        this.antibodiesUsed = other.antibodiesUsed == null ? null : other.antibodiesUsed.copy();
        this.fixationConditions = other.fixationConditions == null ? null : other.fixationConditions.copy();
        this.experimentalInvention = other.experimentalInvention == null ? null : other.experimentalInvention.copy();
        this.distinct = other.distinct;
    }

    @Override
    public MetadataCriteria copy() {
        return new MetadataCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCellLine() {
        return cellLine;
    }

    public StringFilter cellLine() {
        if (cellLine == null) {
            cellLine = new StringFilter();
        }
        return cellLine;
    }

    public void setCellLine(StringFilter cellLine) {
        this.cellLine = cellLine;
    }

    public StringFilter getAntibodiesUsed() {
        return antibodiesUsed;
    }

    public StringFilter antibodiesUsed() {
        if (antibodiesUsed == null) {
            antibodiesUsed = new StringFilter();
        }
        return antibodiesUsed;
    }

    public void setAntibodiesUsed(StringFilter antibodiesUsed) {
        this.antibodiesUsed = antibodiesUsed;
    }

    public StringFilter getFixationConditions() {
        return fixationConditions;
    }

    public StringFilter fixationConditions() {
        if (fixationConditions == null) {
            fixationConditions = new StringFilter();
        }
        return fixationConditions;
    }

    public void setFixationConditions(StringFilter fixationConditions) {
        this.fixationConditions = fixationConditions;
    }

    public StringFilter getExperimentalInvention() {
        return experimentalInvention;
    }

    public StringFilter experimentalInvention() {
        if (experimentalInvention == null) {
            experimentalInvention = new StringFilter();
        }
        return experimentalInvention;
    }

    public void setExperimentalInvention(StringFilter experimentalInvention) {
        this.experimentalInvention = experimentalInvention;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MetadataCriteria that = (MetadataCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(cellLine, that.cellLine) &&
            Objects.equals(antibodiesUsed, that.antibodiesUsed) &&
            Objects.equals(fixationConditions, that.fixationConditions) &&
            Objects.equals(experimentalInvention, that.experimentalInvention) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cellLine, antibodiesUsed, fixationConditions, experimentalInvention, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MetadataCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (cellLine != null ? "cellLine=" + cellLine + ", " : "") +
            (antibodiesUsed != null ? "antibodiesUsed=" + antibodiesUsed + ", " : "") +
            (fixationConditions != null ? "fixationConditions=" + fixationConditions + ", " : "") +
            (experimentalInvention != null ? "experimentalInvention=" + experimentalInvention + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
