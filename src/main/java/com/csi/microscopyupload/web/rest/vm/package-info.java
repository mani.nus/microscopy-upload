/**
 * View Models used by Spring MVC REST controllers.
 */
package com.csi.microscopyupload.web.rest.vm;
