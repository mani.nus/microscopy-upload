package com.csi.microscopyupload.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A Metadata.
 */
@Entity
@Table(name = "metadata")
public class Metadata implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "cell_line", nullable = false)
    private String cellLine;

    @Column(name = "antibodies_used")
    private String antibodiesUsed;

    @Column(name = "fixation_conditions")
    private String fixationConditions;

    @Column(name = "experimental_invention")
    private String experimentalInvention;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Metadata id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCellLine() {
        return this.cellLine;
    }

    public Metadata cellLine(String cellLine) {
        this.setCellLine(cellLine);
        return this;
    }

    public void setCellLine(String cellLine) {
        this.cellLine = cellLine;
    }

    public String getAntibodiesUsed() {
        return this.antibodiesUsed;
    }

    public Metadata antibodiesUsed(String antibodiesUsed) {
        this.setAntibodiesUsed(antibodiesUsed);
        return this;
    }

    public void setAntibodiesUsed(String antibodiesUsed) {
        this.antibodiesUsed = antibodiesUsed;
    }

    public String getFixationConditions() {
        return this.fixationConditions;
    }

    public Metadata fixationConditions(String fixationConditions) {
        this.setFixationConditions(fixationConditions);
        return this;
    }

    public void setFixationConditions(String fixationConditions) {
        this.fixationConditions = fixationConditions;
    }

    public String getExperimentalInvention() {
        return this.experimentalInvention;
    }

    public Metadata experimentalInvention(String experimentalInvention) {
        this.setExperimentalInvention(experimentalInvention);
        return this;
    }

    public void setExperimentalInvention(String experimentalInvention) {
        this.experimentalInvention = experimentalInvention;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Metadata)) {
            return false;
        }
        return id != null && id.equals(((Metadata) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Metadata{" +
            "id=" + getId() +
            ", cellLine='" + getCellLine() + "'" +
            ", antibodiesUsed='" + getAntibodiesUsed() + "'" +
            ", fixationConditions='" + getFixationConditions() + "'" +
            ", experimentalInvention='" + getExperimentalInvention() + "'" +
            "}";
    }
}
