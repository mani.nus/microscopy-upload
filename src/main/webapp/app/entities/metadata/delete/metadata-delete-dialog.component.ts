import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IMetadata } from '../metadata.model';
import { MetadataService } from '../service/metadata.service';

@Component({
  templateUrl: './metadata-delete-dialog.component.html',
})
export class MetadataDeleteDialogComponent {
  metadata?: IMetadata;

  constructor(protected metadataService: MetadataService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.metadataService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
