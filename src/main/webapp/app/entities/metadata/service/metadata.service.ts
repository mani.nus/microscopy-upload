import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IMetadata, getMetadataIdentifier } from '../metadata.model';

export type EntityResponseType = HttpResponse<IMetadata>;
export type EntityArrayResponseType = HttpResponse<IMetadata[]>;

@Injectable({ providedIn: 'root' })
export class MetadataService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/metadata');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(metadata: IMetadata): Observable<EntityResponseType> {
    return this.http.post<IMetadata>(this.resourceUrl, metadata, { observe: 'response' });
  }

  update(metadata: IMetadata): Observable<EntityResponseType> {
    return this.http.put<IMetadata>(`${this.resourceUrl}/${getMetadataIdentifier(metadata) as number}`, metadata, { observe: 'response' });
  }

  partialUpdate(metadata: IMetadata): Observable<EntityResponseType> {
    return this.http.patch<IMetadata>(`${this.resourceUrl}/${getMetadataIdentifier(metadata) as number}`, metadata, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMetadata>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMetadata[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addMetadataToCollectionIfMissing(metadataCollection: IMetadata[], ...metadataToCheck: (IMetadata | null | undefined)[]): IMetadata[] {
    const metadata: IMetadata[] = metadataToCheck.filter(isPresent);
    if (metadata.length > 0) {
      const metadataCollectionIdentifiers = metadataCollection.map(metadataItem => getMetadataIdentifier(metadataItem)!);
      const metadataToAdd = metadata.filter(metadataItem => {
        const metadataIdentifier = getMetadataIdentifier(metadataItem);
        if (metadataIdentifier == null || metadataCollectionIdentifiers.includes(metadataIdentifier)) {
          return false;
        }
        metadataCollectionIdentifiers.push(metadataIdentifier);
        return true;
      });
      return [...metadataToAdd, ...metadataCollection];
    }
    return metadataCollection;
  }
}
