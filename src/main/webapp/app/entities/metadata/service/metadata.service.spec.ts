import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IMetadata, Metadata } from '../metadata.model';

import { MetadataService } from './metadata.service';

describe('Metadata Service', () => {
  let service: MetadataService;
  let httpMock: HttpTestingController;
  let elemDefault: IMetadata;
  let expectedResult: IMetadata | IMetadata[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(MetadataService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      cellLine: 'AAAAAAA',
      antibodiesUsed: 'AAAAAAA',
      fixationConditions: 'AAAAAAA',
      experimentalInvention: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Metadata', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Metadata()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Metadata', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          cellLine: 'BBBBBB',
          antibodiesUsed: 'BBBBBB',
          fixationConditions: 'BBBBBB',
          experimentalInvention: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Metadata', () => {
      const patchObject = Object.assign(
        {
          antibodiesUsed: 'BBBBBB',
          experimentalInvention: 'BBBBBB',
        },
        new Metadata()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Metadata', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          cellLine: 'BBBBBB',
          antibodiesUsed: 'BBBBBB',
          fixationConditions: 'BBBBBB',
          experimentalInvention: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Metadata', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addMetadataToCollectionIfMissing', () => {
      it('should add a Metadata to an empty array', () => {
        const metadata: IMetadata = { id: 123 };
        expectedResult = service.addMetadataToCollectionIfMissing([], metadata);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(metadata);
      });

      it('should not add a Metadata to an array that contains it', () => {
        const metadata: IMetadata = { id: 123 };
        const metadataCollection: IMetadata[] = [
          {
            ...metadata,
          },
          { id: 456 },
        ];
        expectedResult = service.addMetadataToCollectionIfMissing(metadataCollection, metadata);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Metadata to an array that doesn't contain it", () => {
        const metadata: IMetadata = { id: 123 };
        const metadataCollection: IMetadata[] = [{ id: 456 }];
        expectedResult = service.addMetadataToCollectionIfMissing(metadataCollection, metadata);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(metadata);
      });

      it('should add only unique Metadata to an array', () => {
        const metadataArray: IMetadata[] = [{ id: 123 }, { id: 456 }, { id: 30292 }];
        const metadataCollection: IMetadata[] = [{ id: 123 }];
        expectedResult = service.addMetadataToCollectionIfMissing(metadataCollection, ...metadataArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const metadata: IMetadata = { id: 123 };
        const metadata2: IMetadata = { id: 456 };
        expectedResult = service.addMetadataToCollectionIfMissing([], metadata, metadata2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(metadata);
        expect(expectedResult).toContain(metadata2);
      });

      it('should accept null and undefined values', () => {
        const metadata: IMetadata = { id: 123 };
        expectedResult = service.addMetadataToCollectionIfMissing([], null, metadata, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(metadata);
      });

      it('should return initial array if no Metadata is added', () => {
        const metadataCollection: IMetadata[] = [{ id: 123 }];
        expectedResult = service.addMetadataToCollectionIfMissing(metadataCollection, undefined, null);
        expect(expectedResult).toEqual(metadataCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
