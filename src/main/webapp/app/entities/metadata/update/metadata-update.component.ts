import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IMetadata, Metadata } from '../metadata.model';
import { MetadataService } from '../service/metadata.service';

@Component({
  selector: 'csi-metadata-update',
  templateUrl: './metadata-update.component.html',
})
export class MetadataUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    cellLine: [null, [Validators.required]],
    antibodiesUsed: [],
    fixationConditions: [],
    experimentalInvention: [],
  });

  constructor(protected metadataService: MetadataService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ metadata }) => {
      this.updateForm(metadata);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const metadata = this.createFromForm();
    if (metadata.id !== undefined) {
      this.subscribeToSaveResponse(this.metadataService.update(metadata));
    } else {
      this.subscribeToSaveResponse(this.metadataService.create(metadata));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMetadata>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(metadata: IMetadata): void {
    this.editForm.patchValue({
      id: metadata.id,
      cellLine: metadata.cellLine,
      antibodiesUsed: metadata.antibodiesUsed,
      fixationConditions: metadata.fixationConditions,
      experimentalInvention: metadata.experimentalInvention,
    });
  }

  protected createFromForm(): IMetadata {
    return {
      ...new Metadata(),
      id: this.editForm.get(['id'])!.value,
      cellLine: this.editForm.get(['cellLine'])!.value,
      antibodiesUsed: this.editForm.get(['antibodiesUsed'])!.value,
      fixationConditions: this.editForm.get(['fixationConditions'])!.value,
      experimentalInvention: this.editForm.get(['experimentalInvention'])!.value,
    };
  }
}
