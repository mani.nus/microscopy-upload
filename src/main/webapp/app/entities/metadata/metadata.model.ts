export interface IMetadata {
  id?: number;
  cellLine?: string;
  antibodiesUsed?: string | null;
  fixationConditions?: string | null;
  experimentalInvention?: string | null;
}

export class Metadata implements IMetadata {
  constructor(
    public id?: number,
    public cellLine?: string,
    public antibodiesUsed?: string | null,
    public fixationConditions?: string | null,
    public experimentalInvention?: string | null
  ) {}
}

export function getMetadataIdentifier(metadata: IMetadata): number | undefined {
  return metadata.id;
}
