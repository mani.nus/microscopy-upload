import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { MetadataComponent } from './list/metadata.component';
import { MetadataDetailComponent } from './detail/metadata-detail.component';
import { MetadataUpdateComponent } from './update/metadata-update.component';
import { MetadataDeleteDialogComponent } from './delete/metadata-delete-dialog.component';
import { MetadataRoutingModule } from './route/metadata-routing.module';

@NgModule({
  imports: [SharedModule, MetadataRoutingModule],
  declarations: [MetadataComponent, MetadataDetailComponent, MetadataUpdateComponent, MetadataDeleteDialogComponent],
  entryComponents: [MetadataDeleteDialogComponent],
})
export class MetadataModule {}
