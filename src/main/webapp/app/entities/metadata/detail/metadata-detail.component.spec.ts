import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MetadataDetailComponent } from './metadata-detail.component';

describe('Metadata Management Detail Component', () => {
  let comp: MetadataDetailComponent;
  let fixture: ComponentFixture<MetadataDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MetadataDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ metadata: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(MetadataDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(MetadataDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load metadata on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.metadata).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
