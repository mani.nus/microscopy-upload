import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { MetadataComponent } from '../list/metadata.component';
import { MetadataDetailComponent } from '../detail/metadata-detail.component';
import { MetadataUpdateComponent } from '../update/metadata-update.component';
import { MetadataRoutingResolveService } from './metadata-routing-resolve.service';

const metadataRoute: Routes = [
  {
    path: '',
    component: MetadataComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MetadataDetailComponent,
    resolve: {
      metadata: MetadataRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MetadataUpdateComponent,
    resolve: {
      metadata: MetadataRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MetadataUpdateComponent,
    resolve: {
      metadata: MetadataRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(metadataRoute)],
  exports: [RouterModule],
})
export class MetadataRoutingModule {}
