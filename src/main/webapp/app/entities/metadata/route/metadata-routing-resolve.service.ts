import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IMetadata, Metadata } from '../metadata.model';
import { MetadataService } from '../service/metadata.service';

@Injectable({ providedIn: 'root' })
export class MetadataRoutingResolveService implements Resolve<IMetadata> {
  constructor(protected service: MetadataService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMetadata> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((metadata: HttpResponse<Metadata>) => {
          if (metadata.body) {
            return of(metadata.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Metadata());
  }
}
